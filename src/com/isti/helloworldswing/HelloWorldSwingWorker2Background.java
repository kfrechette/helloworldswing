package com.isti.helloworldswing;

import java.util.concurrent.CancellationException;

import javax.swing.SwingWorker;

/**
 * Hello World Swing worker version and separates GUI code from non-GUI code.
 * <p>
 * Runs background code with a {@link javax.swing.SwingWorker}.
 * <p>
 * All classes in this package should not reference any GUI components.
 */
public class HelloWorldSwingWorker2Background
    extends SwingWorker<String, Object> {
  /**
   * NOTE: For long running tasks, the
   * {@link javax.swing.SwingWorker#isCancelled()} method should be called at
   * short intervals to ensure the background task has not been cancelled.
   */
  @Override
  protected String doInBackground() throws Exception {
    if (isCancelled()) {
      throw new CancellationException();
    }
    // get the latest QWClient version number
    return HelloWorldSwingUtils.getLatestVersion();
  }
}
