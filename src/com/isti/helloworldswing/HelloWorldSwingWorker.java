package com.isti.helloworldswing;

import java.util.concurrent.CancellationException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

/**
 * Hello World Swing worker version.
 * <p>
 * Runs background code with a {@link javax.swing.SwingWorker}.
 */
public class HelloWorldSwingWorker extends SwingWorker<String, Object>
    implements HelloWorldSwingConst {
  /**
   * Test program.
   * 
   * @param args the program arguments, not used.
   */
  public static void main(String[] args) {
    try {
      System.out.println("HelloWorldSwing main started");
      System.out.flush();
      // Create the GUI and show it. For thread safety, this method should be
      // invoked from the event-dispatching thread.
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          new HelloWorldSwingWorker();
        }
      });
      System.out.println("HelloWorldSwing main finished");
      System.out.flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private final JLabel text;

  public HelloWorldSwingWorker() {
    // Create and set up the window.
    final JFrame frame = new JFrame("HelloWorldSwing");
    frame.setMinimumSize(MIN_FRAME_SIZE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Add the ubiquitous "Hello World" label.
    final JLabel label = new JLabel("Hello World");
    text = new JLabel("Unknown");
    final JPanel panel = new JPanel();
    panel.add(label);
    panel.add(text);
    frame.getContentPane().add(panel);

    // Display the window.
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    System.out.println("HelloWorldSwing frame created and made visible");
    System.out.flush();
    execute(); // execute the background task on another thread
  }

  /**
   * NOTE: For long running tasks, the
   * {@link javax.swing.SwingWorker#isCancelled()} method should be called at
   * short intervals to ensure the background task has not been cancelled.
   */
  @Override
  protected String doInBackground() throws Exception {
    if (isCancelled()) {
      throw new CancellationException();
    }
    return HelloWorldSwingUtils.getLatestVersion();
  }

  @Override
  protected void done() {
    // Executed on the Event Dispatch Thread (EDT)
    try {
      setText(get());
    } catch (Exception ex) {
      setText(ex.getMessage());
    }
  }

  /**
   * Set the text.
   * 
   * @param s the text.
   */
  public void setText(final String s) {
    // ensure any updates are done on the Event Dispatch Thread (EDT)
    if (!SwingUtilities.isEventDispatchThread()) {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          // this runs on the EDT
          // any objects referenced should be immutable and not by synchronized
          text.setText(s);
        }
      });
    } else {
      text.setText(s);
    }
  }
}
