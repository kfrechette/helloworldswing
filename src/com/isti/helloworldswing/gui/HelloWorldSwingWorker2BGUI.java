package com.isti.helloworldswing.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.isti.helloworldswing.HelloWorldSwingConst;
import com.isti.helloworldswing.HelloWorldSwingWorker2Background;

/**
 * Hello World Swing worker version and separates GUI code from non-GUI code.
 * <p>
 * Runs background code with a {@link javax.swing.SwingWorker} when the
 * <code>Update</code> button is pressed.
 * <p>
 * All classes in this package should run on the event dispatch thread (EDT) and
 * therefore may reference the GUI components.
 * <p>
 * From <a href=
 * "https://docs.oracle.com/javase/tutorial/uiswing/concurrency/dispatch.html">The
 * Event Dispatch Thread</a>:
 * <p>
 * Tasks on the event dispatch thread must finish quickly; if they don't,
 * unhandled events back up and the user interface becomes unresponsive.
 */
public class HelloWorldSwingWorker2BGUI implements HelloWorldSwingConst {
  /**
   * Test program.
   * 
   * @param args the program arguments, not used.
   */
  public static void main(String[] args) {
    try {
      System.out.println("HelloWorldSwing main started");
      System.out.flush();
      // Create the GUI and show it. For thread safety, this method should be
      // invoked from the event-dispatching thread.
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          new HelloWorldSwingWorker2BGUI();
        }
      });
      System.out.println("HelloWorldSwing main finished");
      System.out.flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private HelloWorldSwingWorker2Background _background;
  private final JLabel text;

  /**
   * Create test program.
   */
  public HelloWorldSwingWorker2BGUI() {
    // Create and set up the window.
    final JFrame frame = new JFrame("HelloWorldSwing");
    frame.setMinimumSize(MIN_FRAME_SIZE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // Add the ubiquitous "Hello World" label.
    final JLabel label = new JLabel("Hello World");
    text = new JLabel("Unknown");
    final JPanel panel = new JPanel();
    final JButton update = new JButton("Update");
    final JButton cancel = new JButton("Cancel");
    cancel.setEnabled(false);
    update.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        HelloWorldSwingWorker2Background background = _background;
        // exit if background already created
        if (background != null) {
          return;
        }
        cancel.setEnabled(true);
        update.setEnabled(false);
        background = new HelloWorldSwingWorker2Background() {
          @Override
          protected void done() {
            // Executed on the Event Dispatch Thread (EDT)
            try {
              if (isCancelled()) {
                setText("Cancelled");
              } else {
                setText(get());
              }
            } catch (Exception ex) {
              setText(ex.getMessage());
            } finally {
              _background = null;
              cancel.setEnabled(false);
              update.setEnabled(true);
            }
          }
        };
        _background = background;
        background.execute(); // execute the background task on another thread
      }

    });
    cancel.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        final HelloWorldSwingWorker2Background background = _background;
        if (background != null) {
          background.cancel(true);
        }
      }
    });
    panel.add(label);
    panel.add(text);
    panel.add(update);
    panel.add(cancel);
    frame.getContentPane().add(panel);

    // Display the window.
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    System.out.println("HelloWorldSwing frame created and made visible");
    System.out.flush();
  }

  /**
   * Set the text.
   * 
   * @param s the text.
   */
  public void setText(final String s) {
    // updates will be on the EDT since all of this code runs on the EDT.
    text.setText(s);
  }
}
