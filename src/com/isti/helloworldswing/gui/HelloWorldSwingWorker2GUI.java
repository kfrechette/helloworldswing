package com.isti.helloworldswing.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.isti.helloworldswing.HelloWorldSwingConst;
import com.isti.helloworldswing.HelloWorldSwingWorker2Background;

/**
 * Hello World Swing worker version and separates GUI code from non-GUI code.
 * <p>
 * Runs background code with a {@link javax.swing.SwingWorker}.
 * <p>
 * All classes in this package should run on the event dispatch thread (EDT) and
 * therefore may reference the GUI components.
 * <p>
 * From <a href=
 * "https://docs.oracle.com/javase/tutorial/uiswing/concurrency/dispatch.html">The
 * Event Dispatch Thread</a>:
 * <p>
 * Tasks on the event dispatch thread must finish quickly; if they don't,
 * unhandled events back up and the user interface becomes unresponsive.
 */
public class HelloWorldSwingWorker2GUI implements HelloWorldSwingConst {
  /**
   * Test program.
   * 
   * @param args the program arguments, not used.
   */
  public static void main(String[] args) {
    try {
      System.out.println("HelloWorldSwing main started");
      System.out.flush();
      // Create the GUI and show it. For thread safety, this method should be
      // invoked from the event-dispatching thread.
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          new HelloWorldSwingWorker2GUI();
        }
      });
      System.out.println("HelloWorldSwing main finished");
      System.out.flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private final JLabel text;

  /**
   * Create test program.
   */
  public HelloWorldSwingWorker2GUI() {
    // Create and set up the window.
    final JFrame frame = new JFrame("HelloWorldSwing");
    frame.setMinimumSize(MIN_FRAME_SIZE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // Add the ubiquitous "Hello World" label.
    final JLabel label = new JLabel("Hello World");
    text = new JLabel("Unknown");
    final JPanel panel = new JPanel();
    panel.add(label);
    panel.add(text);
    frame.getContentPane().add(panel);

    // Display the window.
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    System.out.println("HelloWorldSwing frame created and made visible");
    System.out.flush();
    HelloWorldSwingWorker2Background background = new HelloWorldSwingWorker2Background() {
      @Override
      protected void done() {
        // Executed on the Event Dispatch Thread (EDT)
        try {
          setText(get());
        } catch (Exception ex) {
          setText(ex.getMessage());
        }
      }
    };
    background.execute(); // execute the background task on another thread
  }

  /**
   * Set the text.
   * 
   * @param s the text.
   */
  public void setText(final String s) {
    // updates will be on the EDT since all of this code runs on the EDT.
    text.setText(s);
  }
}
