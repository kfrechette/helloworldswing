package com.isti.helloworldswing;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * Hello World Swing new version.
 * <p>
 * Creates the GUI on the Event Dispatch Thread (EDT) via
 * {@link javax.swing.SwingUtilities#invokeLater(Runnable)}.
 */
public class HelloWorldSwingNew implements HelloWorldSwingConst {
  /**
   * Test program.
   * 
   * @param args the program arguments, not used.
   */
  public static void main(String[] args) {
    try {
      System.out.println("HelloWorldSwing main started");
      System.out.flush();
      // Create the GUI and show it. For thread safety, this method should be
      // invoked from the event-dispatching thread.
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          new HelloWorldSwingNew();
        }
      });
      System.out.println("HelloWorldSwing main finished");
      System.out.flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Create test program.
   */
  public HelloWorldSwingNew() {
    // Create and set up the window.
    final JFrame frame = new JFrame("HelloWorldSwing");
    frame.setMinimumSize(MIN_FRAME_SIZE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Add the ubiquitous "Hello World" label.
    final JLabel label = new JLabel("Hello World");
    frame.getContentPane().add(label);

    // Display the window.
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    System.out.println("HelloWorldSwing frame created and made visible");
    System.out.flush();
  }
}
