package com.isti.helloworldswing;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Hello World Swing old version.
 * <p>
 * Creates the GUI on the main thread.
 */
public class HelloWorldSwingOld implements HelloWorldSwingConst {
  /**
   * Test program.
   * 
   * @param args the program arguments, not used.
   */
  public static void main(String[] args) {
    try {
      System.out.println("HelloWorldSwing main started");
      System.out.flush();
      new HelloWorldSwingOld();
      System.out.println("HelloWorldSwing main finished");
      System.out.flush();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Create test program.
   */
  public HelloWorldSwingOld() {
    // Create and set up the window.
    final JFrame frame = new JFrame("HelloWorldSwing");
    frame.setMinimumSize(MIN_FRAME_SIZE);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    // Add the ubiquitous "Hello World" label.
    final JLabel label = new JLabel("Hello World");
    frame.getContentPane().add(label);

    // Display the window.
    frame.pack();
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    System.out.println("HelloWorldSwing frame created and made visible");
    System.out.flush();
  }
}
