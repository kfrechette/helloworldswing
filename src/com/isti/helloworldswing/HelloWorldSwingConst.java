package com.isti.helloworldswing;

import java.awt.Dimension;
import java.net.URL;

/**
 * Hello World Swing Constants.
 */
public interface HelloWorldSwingConst {
  /** Minimum frame size */
  Dimension MIN_FRAME_SIZE = new Dimension(196, 98);
  /** New version text */
  String NEW_VERSION_TEXT = "newVersion=\"";
  /** QWClient updates URL */
  URL QWCLIENT_UPDATES_URL = createURL(
      "https://quakewatch.isti.com/qwclient/download/updates.xml");

  /**
   * Create the URL.
   * 
   * @param spec the {@code String} to parse as a URL.
   * @return the URL or null if error.
   */
  static URL createURL(String spec) {
    URL url = null;
    try {
      return new URL(spec);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return url;
  }
}
