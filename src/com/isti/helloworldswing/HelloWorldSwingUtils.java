package com.isti.helloworldswing;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Hello World Swing Utility methods.
 */
public class HelloWorldSwingUtils implements HelloWorldSwingConst {
  /**
   * Get the latest QWClient version number.
   * 
   * @return the latest QWClient version number or an error message.
   */
  public static String getLatestVersion() {
    // get the latest QWClient version number
    String s = null;
    try {
      try (InputStream in = QWCLIENT_UPDATES_URL.openStream()) {
        byte[] bytes = in.readAllBytes();
        s = new String(bytes, StandardCharsets.UTF_8);
        int beginIndex = s.indexOf(NEW_VERSION_TEXT);
        if (beginIndex != -1) {
          beginIndex += NEW_VERSION_TEXT.length();
          int endIndex = s.indexOf('"', beginIndex);
          if (endIndex != -1) {
            s = s.substring(beginIndex, endIndex);
          }
        }
      }
    } catch (Exception ex) {
      s = ex.toString();
    }
    return s;
  }
}
