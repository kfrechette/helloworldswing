# HelloWorldSwing

How to use Java Swing

## Description

When reading on how to set up a Java Swing program there are a lot of examples that are not entirely correct. The examples below show various ways of setting up a Java Swing program.

## Getting started

This project is an Eclipse project. To get the project into Eclipse select "File" from the top menu, then select "Import...", "Git", "Projects from Git" and "Clone URI".

Enter the URI from gitlab which may be determined by pressing "Clone" button and then the "Copy URL" button under "Clone with SSH".

Press "Next" and accept all of the defaults.

## List of Classes
1. HelloWorldSwingOld

   This class shows a typical example of how to use Java Swing.
   
1. HelloWorldSwingNew

   This class shows another example of how to use Java Swing.
   
1. HelloWorldSwingWorker

   This class shows an example of using a Java Swing SwingWorker. This program gets the latest CISN Display version text.

1. HelloWorldSwingWorker2GUI

   This class shows another example of using a Java Swing SwingWorker. This program does the same as above but separates GUI code from non-GUI code.

1. HelloWorldSwingWorker2BGUI

   This class shows another example of using a Java Swing SwingWorker. This program does the same as above but adds an "Update" and "Cancel" option rather than getting the latest CISN Display version text once at startup.
